"""
Blazegraph Sample Utils
-----------------------

Functions used to derive and test samples of WDQS queries.

Contents:
    _check_get_sample_col_suffix,
    _get_query_time_class_case_when,
    _get_query_length_class_case_when,
    get_query_week_value_counts_query,
    get_query_count_value_counts_query,
    get_query_length_value_counts_query,
    get_query_time_value_counts_query,
    get_query_status_value_counts_query,
    get_sample_blazegraph_queries_query,
    check_append_to_blazegraph_queries_sample,
    get_size_time_sample_blazegraph_queries_query
"""

import os

import pandas as pd
import pyspark

def _check_get_sample_col_suffix(is_sample: bool):
    """
    Returns the suffix '_sample' if the argument is true.
    """
    if is_sample:
        return "_sample"
    else:
        return ""


def _get_query_time_class_case_when():
    """
    Converts 'query_time' into standard 'query_time_class' values.
    """
    return """
CASE
    WHEN query_time < 10 THEN '1_less_10ms'
    WHEN query_time >= 10 AND query_time < 100 THEN '2_10ms_to_100ms'
    WHEN query_time >= 100 AND query_time < 1000 THEN '3_100ms_to_1s'
    WHEN query_time >= 1000 AND query_time < 10000 THEN '4_1s_to_10s'
    WHEN query_time >= 10000 THEN '5_more_10s'
END
"""


def _get_query_length_class_case_when():
    """
    Converts a 'query' into standard 'query_length_class' values.
    """
    return """
CASE
    WHEN length(query) >= 0 AND length(query) <= 50 THEN '1_0_to_50_chars'
    WHEN length(query) >= 51 AND length(query) <= 100 THEN '2_51_to_100_chars'
    WHEN length(query) >= 101 AND length(query) <= 150 THEN '3_101_to_150_chars'
    WHEN length(query) >= 151 AND length(query) <= 200 THEN '4_151_to_200_chars'
    WHEN length(query) >= 201 AND length(query) <= 250 THEN '5_201_to_250_chars'
    ELSE '6_250+_chars'
END
"""


def get_query_week_value_counts_query(df: str, is_sample: bool):
    """
    Derives value counts of queries per week given a df as defined within the function.
    """
    col_suffix = _check_get_sample_col_suffix(is_sample=is_sample)

    return f"""
WITH weeks_of_queries AS (
    SELECT
        CASE
            WHEN month = 10 AND day >= 2 AND day <= 8 THEN '1_10_2_to_10_8'
            WHEN month = 10 AND day >= 9 AND day <= 15 THEN '2_10_9_to_10_15'
            WHEN month = 10 AND day >= 16 AND day <= 22 THEN '3_10_16_to_10_22'
            WHEN month = 10 AND day >= 23 AND day <= 29 THEN '4_10_23_to_10_29'
            WHEN (
                    month = 10 AND day >= 30 AND day <= 31
                ) OR (
                    month = 11 AND day >= 1 AND day <= 5
                ) THEN '5_10_30_to_11_5'
            WHEN month = 11 AND day >= 6 AND day <= 12 THEN '6_11_6_to_11_12'
            WHEN month = 11 AND day >= 13 AND day <= 19 THEN '7_11_13_to_11_19'
            WHEN month = 11 AND day >= 20 AND day <= 26 THEN '8_11_20_to_11_26'
        END AS week

    FROM
        {df}
)

SELECT
    week AS week,
    format_number(count(week), 0) AS total_queries{col_suffix},
    (count(week) / sum(count(week)) OVER ()) * 100 AS percent_of_queries{col_suffix}

FROM
    weeks_of_queries

WHERE
    week IS NOT NULL

GROUP BY
    week

ORDER BY
    week ASC
"""


def get_query_count_value_counts_query(df: str, is_sample: bool):
    """
    Derives value counts of query counts given a df.
    """
    col_suffix = _check_get_sample_col_suffix(is_sample=is_sample)

    return f"""
WITH counts_per_query AS (
    SELECT
        query AS query,
        count(query) AS query_count

    FROM
        {df}

    GROUP BY
        query
)

SELECT
    query_count AS query_count,
    format_number(count(query_count), 0) AS total_queries{col_suffix},
    (count(query_count) / sum(count(query_count)) OVER ()) * 100 AS percent_of_queries{col_suffix}

FROM
    counts_per_query

GROUP BY
    query_count

ORDER BY
    percent_of_queries{col_suffix} DESC
"""


def get_query_length_value_counts_query(df: str, is_sample: bool):
    """
    Derives value counts of query length classes given a df.
    """
    col_suffix = _check_get_sample_col_suffix(is_sample=is_sample)

    return f"""
WITH lengths_of_queries AS (
    SELECT
        {_get_query_length_class_case_when()} AS query_length_class

    FROM
        {df}
)

SELECT
    query_length_class AS query_length_class,
    format_number(count(query_length_class), 0) AS total_queries{col_suffix},
    (count(query_length_class) / sum(count(query_length_class)) OVER ()) * 100 AS percent_of_queries{col_suffix}

FROM
    lengths_of_queries

WHERE
    query_length_class IS NOT NULL

GROUP BY
    query_length_class

ORDER BY
    query_length_class ASC
"""


def get_query_time_value_counts_query(df: str, is_sample: bool):
    """
    Derives value counts of query time classes given a df.
    """
    col_suffix = _check_get_sample_col_suffix(is_sample=is_sample)

    return f"""
WITH times_of_queries AS (
    SELECT
        {_get_query_time_class_case_when()} AS query_time_class

    FROM
        {df}
)

SELECT
    query_time_class AS query_time_class,
    format_number(count(query_time_class), 0) AS total_queries{col_suffix},
    (count(query_time_class) / sum(count(query_time_class)) OVER ()) * 100 AS percent_of_queries{col_suffix}

FROM
    times_of_queries

WHERE
    query_time_class IS NOT NULL

GROUP BY
    query_time_class

ORDER BY
    query_time_class ASC
"""


def get_query_status_value_counts_query(df: str, is_sample: bool):
    """
    Derives value counts of query HTTP statuses given a df.
    """
    col_suffix = _check_get_sample_col_suffix(is_sample=is_sample)

    return f"""
SELECT
    http_status AS http_status,
    format_number(count(http_status), 0) AS total_queries{col_suffix},
    (count(http_status) / sum(count(http_status)) OVER ()) * 100 AS percent_of_queries{col_suffix}

FROM
    {df}

GROUP BY
    http_status

ORDER BY
    percent_of_queries{col_suffix} DESC
"""


def get_sample_blazegraph_queries_query(
    tool: str, where_clause: str, sample_size: int, oversample_multiple: int
):
    """
    Derives a sample of queries for a given tool according to a provided WHERE clause.

    Note: -1 can be passed to 'sample_size' to return the whole sample.
    Note: 'oversample_multiple' should be set to 1 unless the 'sample_size' isn't being met.
    """
    if sample_size != -1:
        join_clause = f"""
LEFT JOIN
    date_percentiles AS p

ON
    b.date = p.date

WHERE
    rand() < p.ratio_of_queries

DISTRIBUTE BY
    rand()

-- SORT BY
--     rand()

LIMIT
    {sample_size}
        """
        broadcast_hint = "/*+ BROADCAST(date_percentiles) */"

    else:
        join_clause = """
DISTRIBUTE BY
    rand()

-- SORT BY
--     rand()
        """
        broadcast_hint = ""

    if oversample_multiple != 1:
        i = 0
        from_clause = """
FROM ("""

        while i < oversample_multiple:
            from_clause += f"""
    SELECT
        b_{i}.id AS id,
        b_{i}.query AS query,
        b_{i}.user_agent AS user_agent,
        b_{i}.http_status AS http_status,
        b_{i}.tool AS tool,
        b_{i}.query_time AS query_time,
        b_{i}.date AS date,
        b_{i}.month AS month,
        b_{i}.day AS day

    FROM
        base_tool_queries AS b_{i}
"""
            i += 1

            if i != oversample_multiple:
                from_clause += """
    UNION ALL
            """

        from_clause += """
) AS b"""

    else:
        from_clause = """
FROM
    base_tool_queries AS b
        """

    sample_query = f"""
WITH base_tool_queries AS (
    SELECT
        meta['id'] AS id,
        query AS query,
        http.request_headers['user-agent'] AS user_agent,
        http.status_code AS http_status,
        '{tool}' AS tool,
        split(meta['dt'], 'T')[0] AS date,
        query_time AS query_time,
        month AS month,
        day AS day

    FROM
        event.wdqs_external_sparql_query

    WHERE {where_clause}
),

date_percentiles AS (
    SELECT
        date AS date,
        (count(date) / sum(count(date)) OVER ()) AS ratio_of_queries

    FROM
        base_tool_queries

    GROUP BY
        date
)

SELECT
    {broadcast_hint}
    b.id AS id,
    b.query AS query,
    b.user_agent AS user_agent,
    b.http_status AS http_status,
    b.tool AS tool,
    -- Returned for testing purposes and will be removed from sample.
    b.query_time AS query_time,
    b.month AS month,
    b.day AS day
{from_clause}
{join_clause}
"""

    print("Creating a sample of the data with the following query:\n" + sample_query)

    return sample_query


def check_append_to_blazegraph_queries_sample(df: pyspark.sql.dataframe.DataFrame, tool: str, file_name: str):
    """
    Checks if data for a given tool is present in the current queries sample output before adding it in.
    """
    pd_df = df.toPandas()

    if os.path.isfile(file_name):
        df_current_sample = pd.read_csv(file_name)
        print(f"A sample for the given 'file_name' already exists. Its size is {len(df_current_sample):,} queries.")

        if tool in list(df_current_sample["tool"].unique()):
            print("Queries for the given 'tool' are already in the sample set. Removing them and replacing with the data provided.")

            df_current_sample = df_current_sample[
                df_current_sample["tool"] != tool
            ]
            df_current_sample = pd.concat([df_current_sample, pd_df])

        else:
            print("Queries for the given 'tool' are not already in the sample set. Appending the data provided.")

            df_current_sample = pd.concat([df_current_sample, pd_df])

        print("Replacing the old CSV with the new data.")
        df_current_sample.to_csv(file_name, sep=",", encoding="utf-8", index=False)

    else:
        print("No sample exists with the given file name. Saving the data as a CSV.")
        df_current_sample = pd_df.copy()
        df_current_sample.to_csv(file_name, sep=",", encoding="utf-8", index=False)

    print(f"Total queries in the sample is now {len(df_current_sample):,}.")


def get_size_time_sample_blazegraph_queries_query(sample_size: int = 20000):
    """
    Returns a query for a sample of queries based on query size and the time it took to ran.
    """
    return f"""
WITH base_queries AS (
    SELECT
        meta['id'] AS id,
        query AS query,
        http.request_headers['user-agent'] AS user_agent,
        http.status_code AS http_status,
        {_get_query_length_class_case_when()} AS query_length_class,
        {_get_query_time_class_case_when()} AS query_time_class,
        query_time AS query_time,
        month AS month,
        day AS day

    FROM
        event.wdqs_external_sparql_query

    WHERE
        year = 2023
        AND (
            month = 11
            OR month = 12
        )
),

size_time_percentiles AS (
    SELECT
        query_length_class AS query_length_class,
        query_time_class AS query_time_class,
        (count(*) / sum(count(*)) OVER ()) AS ratio_of_queries

    FROM
        base_queries

    GROUP BY
        query_length_class,
        query_time_class
)

SELECT
    /*+ BROADCAST(size_time_percentiles) */
    b.id AS id,
    b.query AS query,
    b.user_agent AS user_agent,
    b.http_status AS http_status,
    b.query_time AS query_time,
    -- Returned for testing purposes and will be removed from sample.
    b.month AS month,
    b.day AS day

FROM
    base_queries AS b

LEFT JOIN
    size_time_percentiles AS p

ON
    b.query_length_class = p.query_length_class
    AND b.query_time_class = p.query_time_class

WHERE
    rand() < p.ratio_of_queries

DISTRIBUTE BY
    rand()

-- SORT BY
--     rand()

LIMIT
    {sample_size}
"""
