# Search Platform Notebooks

A place to store jupyter notebooks related to projects owned by the Search Platform Team.

**WARNING**: Before uploading a notebook be sure to triple check that it does not contain any PII data.

## Contents

- wdqs/
  - T349512_representative_wikidata_query_samples/
    - Task: [T349512](https://phabricator.wikimedia.org/T349512)
    - Deriving and analyzing samples of WDQS queries in relation to the Blazegraph split
