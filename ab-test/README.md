# Notebook for performing analysis of cirrus AB tests

## Prepare the environment

```
# Init conda
source /opt/conda-analytics/etc/profile.d/conda.sh

# Whatever conda environment jupyter uses. Could probably create
# a custom one?
conda activate 2023-03-27T19.59.58_ebernhardson

# The default conda installer is dreadfully slow, bring in mamba
# TODO: Might need special conda-forge arguments
conda install mamba

# Not clear why pyyaml isn't already here, but we need it
mamba install quarto pyyaml
```

## Prepare the notebook

First clone this repository onto one of the stat machines and change
your current working directory into the directory containing the notebook.
You may also need to `kinit` to have valid kerberos credentials.

While the notebook is mostly ready to go, there are still a few prose updates
that must be manually performed before running the analysis. You will want
to open the notebook in jupyterlab and make the following updates:

* Update `title` and `author` section in the first cell as appropriate
* Update the content of the `Introduction` section to describe the test
 that was performed.


## Execute analysis and generate html report

Once the report is prepared run the following command, once for each wiki, to
render the report. When analyzing multiple wikis a simple bash for loop will
usually suffice.

```
https_proxy=http://webproxy.eqiad.wmnet:8080 \
    quarto render \
    T377128-AB-Test-Metrics.ipynb \
    --execute \
    --to html \
    -P "WIKI:$wiki" \
    -P "START_AT:2024-12-10" \
    -P "END_AT:2024-12-17" \
    --output "T377128-AB-Test-Metrics-WIKI=${wiki}.html"
